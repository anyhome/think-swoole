ThinkPHP 5.1 Swoole 扩展
===============

## 安装

首先按照Swoole官网说明安装swoole扩展
然后使用
composer require ayhome/swoole

## 使用方法

### Server
直接通过 URL 构造访问，例如    http://127.0.0.1:9502/swoole/index/index?url=2323
则访问 swoole 模块下面的 index 控制器的 index 方法，
实现参数绑定
~~~
### HttpServer

命令行下启动服务端
~~~
php think swoole [start|stop|reload|restart]
~~~

swoole的参数可以在应用配置目录下的swoole.php里面配置。

由于onWorkerStart运行的时候没有HTTP_HOST，因此最好在应用配置文件中设置app_host