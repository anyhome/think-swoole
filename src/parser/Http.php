<?php


namespace ayhome\swoole\parser;
use Swoole\Process;

use think\console\Command;
use think\console\Output;
use think\console\Input;

class Http
{
  public function request($request, $response){
    $path = $request->server['path_info'];
    $get = $request->get;
    $post = $request->post;

    $path_arr = explode("/", $path);

    $group = $path_arr[1];
    $controller = ucwords(strtolower($path_arr[2]));
    $action = $path_arr[3];
    
    $cls = "\\app\\{$group}\\controller\\{$controller}";
    $m = new \app\swoole\controller\Index();

    if (!class_exists($cls)) {
      $response->header("Content-Type", "text/plain");
      $response->end("{$cls} 类不存在 \n");
    }
    
    if (!method_exists($cls, $action)) {
      $response->header("Content-Type", "text/plain");
      $response->end("{$cls}\{$cls} 方法不存在 \n");
    }

    if ($get && $post) {
      $bindParams = array_merge($get,$post);
    }elseif ($get) {
      $bindParams = $get;
    }elseif ($post) {
      $bindParams = $post;
    }else{
      $bindParams = array();
    }
    $bindParams['request'] = $request;

    $ret = call_user_func_array([$cls, $action], $bindParams);

    $response->header("Content-Type", "text/plain");
    $response->end("Hellodfd  {$ret} \n");

  }

  public function empty(){
    $response->header("Content-Type", "text/plain");
    $response->end("empty \n");
  }
}