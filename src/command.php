<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

\think\Console::addDefaultCommands(['\\ayhome\\swoole\\command\\Swoole']);

\think\Facade::bind([
    \ayhome\swoole\facade\Application::class => \ayhome\swoole\Application::class,
    \ayhome\swoole\facade\Swoole::class      => \ayhome\swoole\Swoole::class,
]);
